---
linktitle: หลักการปัดเศษทศนิยม
summary: 
title: หลักการปัดเศษทศนิยทมในแต่ละยอดรวม สำหรับการคิดค่าภาษีอากร
date: "2020-05-19T00:00:00Z"
lastmod: "2020-05-19T00:00:00Z"
draft: false  # Is this a draft? true/false
toc: yes # Show table of contents? true/false
type: docs  # Do not modify.

menu:
  guide:
    parent: คู่มือพิธีการนำเข้า 
    weight: 28

weight: 28
---

![](https://ecs-support.github.io/KM/customs/post/knowledge/rounding-decimal/rounding-decimal.png)