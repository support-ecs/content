---
linktitle: การยื่นใบขนสินค้าในรูปแบบเอกสาร
title: การจัดทำและยื่นใบขนสินค้าในรูปแบบเอกสาร (Manual)
date: "2020-05-09T00:00:00Z"
lastmod: "2020-05-09T00:00:00Z"
draft: false  # Is this a draft? true/false
toc: false  # Show table of contents? true/false
type: docs  # Do not modify.

menu:
  export:
    parent: 3. คู่มือพิธีการส่งออกสินค้า
    weight: 29
weight: 29
---

![](../img/e-Export-guidejpg_Page52.jpg)