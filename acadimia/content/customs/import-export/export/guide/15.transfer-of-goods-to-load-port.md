---
linktitle:  การขนย้ายสินค้าผ่านสถานีรับบรรทุก
title:  การขนย้ายสินค้าผ่านสถานีรับบรรทุก ณ ด่านศุลกากรที่ส่งออก
date: "2020-05-09T00:00:00Z"
lastmod: "2020-05-09T00:00:00Z"
draft: false  # Is this a draft? true/false
toc: true  # Show table of contents? true/false
type: docs  # Do not modify.

menu:
  export:
    parent: 3. คู่มือพิธีการส่งออกสินค้า
    weight: 32
weight: 32
---

![](../img/e-Export-guidejpg_Page80.jpg)

![](../img/e-Export-guidejpg_Page81.jpg)