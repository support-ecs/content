---
linktitle: Import
# Page metadata.
title: Import
date: "2020-07-22T00:00:00Z"
lastmod: "2020-07-22T00:00:00Z"
draft: false  # Is this a draft? true/false
toc: false # Show table of contents? true/false
type: docs  # Do not modify.

# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.

menu:
  faq:
    parent: FAQ.
    weight: 2

weight: 2
---

![](deducted.jpg)

{{< collapsible "การบันทึกข้อมูล ราคาขายปลีกแนะนำในใบขนสินค้า">}}

## การบันทึกข้อมูลราคาขายปลีกแนะนำในใบขนสินค้า


**คำตอบ :**  

ให้ระบุราคาขายปลีกแนะนำ ตามแบบแจ้งราคาขายปลีกแนะนำ (ภส.02-01) ในช่อง Deducted Amount ยกเว้นกรณี

- กรณีที่อัตราภาษีสรรพสามิตกำหนด**อัตราภาษีตามปริมาณเพียงอย่างเดียว** ให้ระบุ **_0.1_**
- กรณีของ**นำเข้าคลังสินค้าทัณฑ์บน เขตปลอดอากร เขตประกอบการเสรี** และของที่**ได้รับยกเว้นอากรตามภาค 4**รวมทั้งของที่**โอนย้ายระหว่างคลังสินค้าทัณฑ์บน เขตปลอดอากร เขตประกอบการเสรีให้ระบุ** **_0.2_** 

![](../img/deducted-amount.jpg)


> ที่มา : กรมสรรพสามิต  

{{< /collapsible >}}



