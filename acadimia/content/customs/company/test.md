---
title: Example Page 1
date: 2020-05-05
type: docs
---
Pages
You can create as many pages as your need. Each page should have a title, and a page type of book.

title is the page title which appears in page header, whereas linktitle is the label for link to this page. If you remove linktitle, the menu link will display the page title.

To remove the right sidebar for table of contents (generated from the headings on your page), set toc to false.

To show a prev/next pager at the bottom of each docs section page, enable docs_section_pager in params.toml and then set the order of the pager if you wish by defining a weight for each page.

An example page is as follows:



----

## toc

{{% toc %}}


-----

## List Tags
 {{< list_tags >}}




 ------


 ## List Categories

 {{< list_categories >}}