---
title: พระราชบัญญัติศุลกากร พ.ศ.2560
subtitle: พระราชบัญญัติศุลกากร พ.ศ.2560 (ภาษาไทย)
summary: พระราชบัญญัติศุลกากร พ.ศ.2560 (ภาษาไทย)
authors:
- Admin
tags: ["กรมศุลกากร"]
categories: ["ประกาศ/กฎหมาย","พระราชบัญญัติศุลกากร"]
date: "2017-11-16"
lastMod: "2017-11-16"
featured: true
draft: false


image:
  placement: 
  caption: 
  focal_point: ""
  preview_only: true

---

![](featured.png)

<br>
{{< gdocs src="./CUSTOMS-ACT.pdf" >}}

<br>

> ที่มา : [กรมศุลกากร](./CUSTOMS-ACT.pdf)