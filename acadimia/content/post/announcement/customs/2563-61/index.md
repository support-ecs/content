---
title: พิธีการศุลกากรในการนำเข้าหน้ากากอนามัยและวัตถุดิบที่นำเข้ามาเพื่อผลิต
subtitle: ประกาศกรมศุลกากรที่ 61/.2563 เรื่อง แก้ไขเพิ่มเติมประกาศกรมศุลกากรที่ 144/2560 เรื่อง หลักเกณฑ์และพิธีการสำหรับการลดอัตราอากรและยกเว้นอากรศุลกากรตามมาตรา 12 แห่งพระราชกำหนดพิกัดอัตราศุลกากร พ.ศ.2530
summary: ประกาศกรมศุลกากรที่ 61/.2563 เรื่อง แก้ไขเพิ่มเติมประกาศกรมศุลกากรที่ 144/2560 เรื่อง หลักเกณฑ์และพิธีการสำหรับการลดอัตราอากรและยกเว้นอากรศุลกากรตามมาตรา 12 แห่งพระราชกำหนดพิกัดอัตราศุลกากร พ.ศ.2530
authors:
- admin
tags: ["หน้ากากอนามัย","ลดอัตราอากร","การนำเข้า"]
categories: ["ประกาศกรมศุลกากร","ประกาศ/กฎหมาย"]
date: "2020-03-26"
lastMod: "2020-03-26"
featured: true
draft: false
toc: true


image:
  placement: 
  caption: 
  focal_point: ""
  preview_only: true

---

![](featured.jpg)

![](./2563-61.jpg)

<br>

ประกาศกรมศุลกากรที่ 61/.2563 เรื่อง แก้ไขเพิ่มเติมประกาศกรมศุลกากรที่ 144/.2560 เรื่อง หลักเกณฑ์และพิธีการสำหรับการลดอัตราอากรและยกเว้นอากรศุลกากรตามมาตรา 12 แห่งพระราชกำหนดพิกัดอัตราศุลกากร พ.ศ.2530 มีผลบังคับใช้ **_ตั้งแต่วันที่ 26 มีนาคม 2563 เป็นต้นไป_**
<br>
{{< gdocs src="./2563-61.pdf" >}}

<br>

 <a href="./2563-61.pdf" target="_blank" id="download_files">ดาวน์โหลดประกาศ 
                <i class=" fas fa-file-pdf" style="font-size:30px; color: red;" id="icon_download"></i>
            </a>

  <a href="./attach_2563-61.docx" target="_blank" id="download_files">ดาวน์โหลดเอกสารแนบท้าย 
                <i class=" fas fa-file-word" style="font-size:30px; color: blue;" id="icon_download"></i>
            </a>  

<style>

#download_files:hover,
#icon_downlad:hover {
  color: #2A4B7C; 
  font-weight:bold;
  text-decoration: none;
  transition: color 0.4s ease;
}

</style>


> ที่มา : [กรมศุลกากร](http://www.customs.go.th/cont_strc_slide_image.php?current_id=14232832414a505f46464a4e464b47)