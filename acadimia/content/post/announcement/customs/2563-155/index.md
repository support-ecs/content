---
title: รายชื่อประเทศที่ได้รับสิทธิสำหรับเขตการค้าเสรีอาเซียน-ฮ่องกง    
subtitle:  ประกาศกรมศุลกากรที่ 155/.2563 เรื่อง แก้ไขเพิ่มเติมประกาศกรมศุลกากรที่ 89/.2562 
summary:  รายชื่อประเทศที่ได้รับสิทธิยกเว้นอากรและลดอัตราอากรศุลกากรสำหรับเขตการค้าเสรีอาเซียน-ฮ่องกง
authors:
    - admin
tags: ["อาเซียน-ฮ่องกง"]
categories: ["สิทธิประโยชน์ทางการค้า"]
date: "2020-10-09"
lastMod: "2020-10-09"
featured: true
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Placement options: 1 = Full column width, 2 = Out-set, 3 = Screen-width
# Focal point options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
image:
  placement: 
  caption: ''
  focal_point: ""
  preview_only: true

---

![](featured.jpg)

ประกาศกรมศุลกากรที่ 155/.2563 เรื่อง แก้ไขเพิ่มเติมประกาศกรมศุลกากรที่ 89/.2562 (หลักเกณฑ์และพิธีการการยกเว้นอากรและลดอัตราอากรศุลกากรสำหรับเขตการค้าเสรี*อาเซียน-ฮ่องกง*) มีการแก้ไขเพิ่มเติม รายชื่อประเทศที่ได้รับสิทธิยกเว้นอากรและลดอัตราอากรศุลกากรสำหรับเขตการค้าเสรีอาเซียน-ฮ่องกง

![](img-01.jpg)

<br>

<a href="./2563-155.pdf" target="_blank" id="download_files">Download  <i class=" fas fa-file-pdf"></i> </a>




> ที่มา : [กรมศุลกากร](http://www.customs.go.th/cont_strc_download_with_docno_date.php?lang=th&top_menu=menu_homepage&current_id=14232832414c505f46464a4f464b4d)

