---
title: รายชื่อประเทศสมาชิกอาเซียนที่สามารถออกหนังสือรับรองถิ่นกำเนิดสินค้า (Form D) ลงนามอยู่ในรูปแบบอิเล็กทรอนิกส์
subtitle: รายชื่อประเทศสมาชิกอาเซียนที่สามารถออกหนังสือรับรองถิ่นกำเนิดสินค้า (Form D) ที่มีตราประทับและลายมือชื่อของผู้มีอำนาจลงนามอยู่ในรูปแบบอิเล็กทรอนิกส์ 
summary: ประกาศกองพิกัดอัตราศุลกากรที่ 8/.2563 เรื่อง รายชื่อประเทศสมาชิกอาเซียนที่สามารถออกหนังสือรับรองถิ่นกำเนิดสินค้า (Form D) ที่มีตราประทับและลายมือชื่อของผู้มีอำนาจลงนามอยู่ในรูปแบบอิเล็กทรอนิกส์ ภายใต้ความตกลงการค้าสินค้าของอาเซียน.  
authors:
- admin
tags: ["อาเซียน","ATIGA"]
categories: ["ประกาศ/กฎหมาย","ประกาศกองพิกัดอัตราศุลกากร"]
date: "2020-09-28"
lastMod: "2020-09-28"
featured: true
draft: false

image:
  placement: 
  caption: 
  focal_point: ""
  preview_only: true
---


![](img-01.png)

**ประกาศกองพิกัดอัตราศุลกากรที่ 8/.2563 เรื่อง รายชื่อประเทศสมาชิกอาเซียนที่สามารถออกหนังสือรับรองถิ่นกำเนิดสินค้า (Form D) ที่มีตราประทับและลายมือชื่อของผู้มีอำนาจลงนามอยู่ในรูปแบบอิเล็กทรอนิกส์ ภายใต้ความตกลงการค้าสินค้าของอาเซียน**

มีผลบังคับตั้งแต่*วันที่ 22 กันยายน 2563* เป็นต้นไป

![](img.jpg)

<br>
{{< gdocs src="./2563-08.pdf" >}}

<br>



 <a href="./2563-08.pdf" target="_blank" id="download_files">ดาวน์โหลดประกาศ 
                <i class=" fas fa-file-pdf" style="font-size:30px; color: red;" id="icon_download"></i>
            </a>

<style>

#download_files:hover  {
  color: #2A4B7C; 
  font-weight:bold;
  text-decoration: none;
  transition: color 0.4s ease;
}

#icon_downlad:hover {
  color:green;
  transition: color 0.4s ease;
}

</style>

ที่มา : [กรมศุลกากร](http://www.customs.go.th/cont_strc_simple_with_date.php?current_id=14232832414c505e4e464b4b464b4c)

