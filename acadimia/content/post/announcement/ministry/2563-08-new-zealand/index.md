---
title: การยกเว้นอากรและลดอัตราอากรศุลกากรสำหรับของที่มีถิ่นกำเนิดจากนิวซีแลนด์
subtitle: ประกาศกระทรวงการคลัง เรื่อง การยกเว้นอากรและลดอัตราอากรศุลกากรสำหรับของที่มีถิ่นกำเนิดจากนิวซีแลนด์ (ฉบับที่ 2 )

summary: ประกาศกระทรวงการคลัง เรื่อง การยกเว้นอากรและลดอัตราอากรศุลกากรสำหรับของที่มีถิ่นกำเนิดจากนิวซีแลนด์ (ฉบับที่ 2 )
authors:
- admin
categories: ["ประกาศ/กฎหมาย","ประกาศกระทรวงการคลัง"]
tags: ["นิวซีแลนด์","TNZ"]
date: "2020-08-04"
lastMod: "2020-08-04"
featured: true
draft: false

image:
  placement: 
  caption: 
  focal_point: ""
  preview_only: true

---

![](featured.png)

## ประกาศกระทรวงการคลัง เรื่อง การยกเว้นอากรและลดอัตราอากรศุลกากรสำหรับของที่มีถิ่นกำเนิดจากนิวซีแลนด์ (ฉบับที่ 2 ) 


![](./img/imgjpg_Page1.jpg)

![](./img/imgjpg_Page2.jpg)



 <a href="./new-zealand.pdf" target="_blank" id="download_files">ดาวน์โหลดประกาศ 
                <i class=" fas fa-file-pdf" style="font-size:30px; color: red;" id="icon_download"></i>
            </a>

<style>

#download_files:hover,
#icon_downlad:hover {
  color: #2A4B7C; 
  font-weight:bold;
  text-decoration: none;
  transition: color 0.4s ease;
}

</style>




